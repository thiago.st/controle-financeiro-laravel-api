<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use SoftDeletes;
    protected $fillable = ['nome'];
    protected $dates    = ['deleted_at'];

    function lancamentos() {
        return $this->hasOne('App\Lancamento');
    }
}