<?php 

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    public function index()
    {
        // $categoria = Categoria::all();
        $categoria = Categoria::with('lancamentos')->get();
        if (!$categoria) {
                return response()->json(['message' => 'Record not found',], 404);
            }
        return response()->json($categoria);
    }

    public function show($id)
    {
        $Categoria = Categoria::find($id);
        if (!$Categoria) {
            return response()->json(['message' => 'Record not found',], 404);
        }
        return response()->json($Categoria);
    }

    public function store(Request $request)
    {
        $Categoria = new Categoria;
        $Categoria->fill($request->all());
        $Categoria->save();

        return response()->json($Categoria, 201);
    }

    public function update(Request $request, $id)
    {
        $Categoria = Categoria::find($id);
        if(!$Categoria) {
            return response()->json(['message' => 'Record not found',], 404);
        }
    
        $Categoria->fill($request->all());
        $Categoria->save();
    
        return response()->json($Categoria);
    }

    public function destroy($id)
    {
        $Categoria = Categoria::find($id);
        if(!$Categoria) {
            return response()->json(['message' => 'Record not found',], 404);
        }
        $Categoria->delete();
        return response()->json(['message' => 'Registro deletado',], 200);
    }
}
