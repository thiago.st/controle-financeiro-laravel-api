<?php

namespace App\Http\Controllers;

use App\Pessoa;
use App\Categoria;
use App\Lancamento;
use Illuminate\Http\Request;

class LancamentoController extends Controller
{
    public function index()
    {
        // $lancamentos = Lancamento::all();
        $lancamentos = Lancamento::with('pessoas', 'categorias')->get();
        if (!$lancamentos) {
                return response()->json(['message' => 'Record not found',], 404);
            }
        return response()->json(['data' => $lancamentos], 200);
    }

    public function show($id)
    {
        $lancamentos = Lancamento::with('pessoas', 'categorias')->get()->find($id);
        if (!$lancamentos) {
            return response()->json(['message' => 'Record not found',], 404);
        }
        return response()->json(['data' => $lancamentos], 200);
    }

    public function store(Request $request)
    {
        $lancamentos = new Lancamento;
        $lancamentos->fill($request->all());
        $lancamentos->save();

        return response()->json($lancamentos, 201);
    }

    public function update(Request $request, $id)
    {
        $lancamentos = Lancamento::find($id);
        if(!$lancamentos) {
            return response()->json(['message' => 'Record not found',], 404);
        }
    
        $lancamentos->fill($request->all());
        $lancamentos->save();
    
        return response()->json($lancamentos);
    }

    public function destroy($id)
    {
        $lancamentos = Lancamento::find($id);
        if(!$lancamentos) {
            return response()->json(['message' => 'Record not found',], 404);
        }
        $lancamentos->delete();
        return response()->json(['message' => 'Registro deletado',], 200);
    }
}
