<?php

namespace App\Http\Controllers;

use App\Pessoa;
use Illuminate\Http\Request;

class PessoaController extends Controller
{
    public function index()
    {
        // $pessoa = Pessoa::all();
        $pessoa = Pessoa::with('lancamentos')->get();
        if (!$pessoa) {
                return response()->json(['message' => 'Record not found',], 404);
            }
        return response()->json($pessoa);
    }

    public function show($id)
    {
        $Pessoa = Pessoa::with('lancamentos')->get()->find($id);
        if (!$Pessoa) {
            return response()->json(['message' => 'Record not found',], 404);
        }
        return response()->json($Pessoa);
    }

    public function store(Request $request)
    {
        $Pessoa = new Pessoa;
        $Pessoa->fill($request->all());
        $Pessoa->save();

        return response()->json($Pessoa, 201);
    }

    public function update(Request $request, $id)
    {
        $Pessoa = Pessoa::find($id);
        if(!$Pessoa) {
            return response()->json(['message' => 'Record not found',], 404);
        }
    
        $Pessoa->fill($request->all());
        $Pessoa->save();
    
        return response()->json($Pessoa);
    }

    public function destroy($id)
    {
        $Pessoa = Pessoa::find($id);
        if(!$Pessoa) {
            return response()->json(['message' => 'Record not found',], 404);
        }
        $Pessoa->delete();
        return response()->json(['message' => 'Registro deletado',], 200);
    }
}
