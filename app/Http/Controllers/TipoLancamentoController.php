<?php

namespace App\Http\Controllers;

use App\TipoLancamento;
use Illuminate\Http\Request;

class TipoLancamentoController extends Controller
{
    public function index()
    {
        // $tipoLancamento = TipoLancamento::all();
        $tipoLancamento = TipoLancamento::with('lancamentos')->get();
        if (!$tipoLancamento) {
                return response()->json(['message' => 'Record not found',], 404);
            }
        return response()->json($tipoLancamento);
    }

    public function show($id)
    {
        $TipoLancamento = TipoLancamento::find($id);
        if (!$TipoLancamento) {
            return response()->json(['message' => 'Record not found',], 404);
        }
        return response()->json($TipoLancamento);
    }

    public function store(Request $request)
    {
        $TipoLancamento = new TipoLancamento;
        $TipoLancamento->fill($request->all());
        $TipoLancamento->save();

        return response()->json($TipoLancamento, 201);
    }

    public function update(Request $request, $id)
    {
        $TipoLancamento = TipoLancamento::find($id);
        if(!$TipoLancamento) {
            return response()->json(['message' => 'Record not found',], 404);
        }
    
        $TipoLancamento->fill($request->all());
        $TipoLancamento->save();
    
        return response()->json($TipoLancamento);
    }

    public function destroy($id)
    {
        $TipoLancamento = TipoLancamento::find($id);
        if(!$TipoLancamento) {
            return response()->json(['message' => 'Record not found',], 404);
        }
        $TipoLancamento->delete();
        return response()->json(['message' => 'Registro deletado',], 200);
    }
}
