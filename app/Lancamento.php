<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Lancamento extends Model
{
    use SoftDeletes;
    protected $fillable = ['descricao', 'dataVencimento','dataPagamento','valor','observacao',
                            'tipo','categoria_id','pessoa_id'];
    protected $dates = ['deleted_at'];

    function pessoas() {
        return $this->belongsTo(Pessoa::class, 'pessoa_id');
    }

    public function categorias()
    {
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }
}
