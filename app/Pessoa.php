<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes; 
use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    use SoftDeletes;
    protected $fillable = ['nome', 'logradouro','numero','complemento','bairro','cep','cidade','estado','ativo'];
    protected $dates = ['deleted_at'];

    function lancamentos() {
        return $this->hasMany('App\Lancamento');
    }
}
