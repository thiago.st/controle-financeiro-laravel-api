<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class TipoLancamento extends Model
{
    use SoftDeletes;
    protected $fillable = ['tipo'];
    protected $dates    = ['deleted_at'];
}
