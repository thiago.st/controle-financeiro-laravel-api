<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    use SoftDeletes;
    protected $fillable = ['logradouro','numero','complemento','bairro','cep','cidade','estado'];
    protected $dates = ['deleted_at'];
}
