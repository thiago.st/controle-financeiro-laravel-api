<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([
            'nome'  => 'Lazer',
        ]);

        DB::table('categorias')->insert([
            'nome'  => 'Alimentação',
        ]);

        DB::table('categorias')->insert([
            'nome'  => 'Supermercado',
        ]);

        DB::table('categorias')->insert([
            'nome'  => 'Farmácia',
        ]);
        
        DB::table('categorias')->insert([
            'nome'  => 'Outros',
        ]);
    }
}