<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        //$this->call(CategoriasTableSeeder::class);
        //$this->call(TipoLancamentoTableSeeder::class);
        //$this->call(PessoaTableSeeder::class);
        //$this->call(EnderecoTableSeeder::class);
        //$this->call(PessoasTableSeeder::class);
        $this->call(LancamentosTableSeeder::class);
    }
}
