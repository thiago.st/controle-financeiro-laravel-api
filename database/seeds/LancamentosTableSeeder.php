<?php

use Illuminate\Database\Seeder;

class LancamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lancamentos')->insert([
            'descricao'  => 'Compras com supermercado',
            'dataVencimento'  => '2018-03-14',
            'dataPagamento'  => '2018-03-14',
            'valor'  => '150',
            'observacao'  => 'Atacadão',
            'tipo'  => 'DESPESA',
            'categoria_id'  => '3',
            'pessoa_id'  => '1',
        ]);

        DB::table('lancamentos')->insert([
            'descricao'  => 'Salário',
            'dataVencimento'  => '2018-03-31',
            'dataPagamento'  => '2018-03-31',
            'valor'  => '300',
            'observacao'  => 'Sálário do mês de março',
            'tipo'  => 'RECEITA',
            'categoria_id'  => '5',
            'pessoa_id'  => '1',
        ]);
    }
}
