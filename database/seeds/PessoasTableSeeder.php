<?php

use Illuminate\Database\Seeder;

class PessoasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pessoas')->insert([
            'nome'  => 'Thiago',
            'logradouro'  => 'Rua Raimundo Nonato de Araújo',
            'numero'  => '58',
            'complemento'  => 'casa',
            'bairro'  => 'José Pinheiro',
            'cep'  => '58407530',
            'cidade'  => 'Campina Grande',
            'estado'  => 'Paraíba',
            'ativo' => true,
        ]);

        DB::table('pessoas')->insert([
            'nome'  => 'Charles',
            'logradouro'  => 'Rua Programador PHP',
            'numero'  => '10',
            'complemento'  => 'apartamento',
            'bairro'  => 'Bodocongó',
            'cep'  => '58400000',
            'cidade'  => 'Campina Grande',
            'estado'  => 'Paraíba',
            'ativo' => true,
        ]);

        DB::table('pessoas')->insert([
            'nome'  => 'Tayrone',
            'logradouro'  => 'Rua José Tayrone',
            'numero'  => '15',
            'complemento'  => 'casa',
            'bairro'  => 'Palmeira',
            'cep'  => '58400000',
            'cidade'  => 'Campina Grande',
            'estado'  => 'Paraíba',
            'ativo' => false,
        ]);
    }
}
