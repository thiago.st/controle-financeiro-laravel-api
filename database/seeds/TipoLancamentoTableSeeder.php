<?php

use Illuminate\Database\Seeder;

class TipoLancamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_lancamentos')->insert([
            'tipo'  => '0',
        ]);

        DB::table('tipo_lancamentos')->insert([
            'tipo'  => '1',
        ]);
    }
}
